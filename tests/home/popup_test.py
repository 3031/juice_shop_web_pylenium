from selenium.webdriver.common.by import By

def test_popup(py):
    py.visit('http://localhost:3000/#/')
    py.contains('Dismiss').click()
    assert py.get('[aria-label="cookieconsent"]').should().be_visible()
    py.get('[aria-label="cookieconsent"]').focus()
    assert py.contains('This website uses fruit cookies to ensure you get the juiciest tracking experience. ').should().be_visible()
    assert py.get('[aria-label="learn more about cookies"]').should().be_visible()
    assert py.get('a[href="https://www.youtube.com/watch?v=9PnbKL3wuH4"]').should().be_visible()
    assert py.get('#cookieconsent\:desc').get('a').should().have_text('But me wait!')
    assert py.get('[aria-label="dismiss cookie message"]').should().be_visible()
    assert py.get('.cc-compliance').get('a').should().have_text('Me want it!')

def test_close_popup(py):
    py.visit('http://localhost:3000/#/')
    py.contains('Dismiss').click()
    py.get('.cc-compliance').get('a').should().have_text('Me want it!').click()
    assert py.wait(5).until(lambda driver: driver.find_element(By.CSS_SELECTOR, '[aria-label="cookieconsent"]').is_displayed() == False)
