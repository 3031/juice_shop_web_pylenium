from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

def test_close_menu(py):
    py.visit('http://localhost:3000/#/')
    py.contains('Dismiss').click()
    assert py.get('sidenav').is_displayed() == False
    py.get('[aria-label="Open Sidenav"]').click()
    assert py.get('sidenav').is_displayed() == True
    driver = py.webdriver
    actionChains = ActionChains(driver)
    hidden_container = py.get('.cdk-visually-hidden.cdk-focus-trap-anchor').webelement
    size = py.window_size
    actionChains.move_to_element(hidden_container).move_by_offset(size["width"] / 2, size["height"] / 2).click().perform()
    assert py.wait(5).until(lambda driver: driver.find_element(By.TAG_NAME, 'sidenav').is_displayed() == False)
