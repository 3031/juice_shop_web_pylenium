from selenium.webdriver.common.by import By

def test_hover_product(py):
    py.visit('http://localhost:3000/#/')
    py.contains('Dismiss').click()
    py.get('[alt="Best Juice Shop Salesman Artwork"]').hover()
    assert py.contains('Click for more information').should().be_visible()
    assert py.get('.mat-tooltip-trigger.product').should().be_visible()
    assert py.get('[mattooltip="Click for more information"]').should().be_visible()
    assert py.get('[mattooltip="Click for more information"]').is_displayed()
    assert py.contains('Click for more information').is_displayed()
