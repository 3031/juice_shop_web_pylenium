from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

def test_pages(py):
    py.visit('http://localhost:3000/#/')
    py.contains('Dismiss').click()
    py.get('.cc-compliance').get('a').should().have_text('Me want it!').click()
    py.get('.mat-paginator-page-size-label').scroll_into_view()
    assert py.contains('King of the Hill').should().be_visible()
    assert py.contains('King of the Hill').is_displayed()
    el = py.get('[aria-label="Next page"]')
    actions = ActionChains(py.webdriver)
    actions.move_to_element(el.webelement).click().perform()
    assert py.wait(5).until(lambda driver: driver.find_element(By.TAG_NAME, 'sidenav').is_displayed() == False)
    el = py.wait(5, use_py=True).until(lambda driver: driver.find_element(By.XPATH, "//div[contains(text(), 'Juice Shop T-Shirt')]"))
    assert el.should().be_visible()
    assert py.contains('Juice Shop T-Shirt').should().be_visible()
    assert py.contains('Juice Shop T-Shirt').is_displayed()
    py.get('.mat-toolbar').scroll_into_view()
    assert py.contains('CTF Girlie-Shirt').should().be_visible()
    assert py.contains('CTF Girlie-Shirt').is_displayed()
