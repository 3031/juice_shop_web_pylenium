from selenium.webdriver.common.by import By
from random import randint

def test_select_items_per_page(py):
    py.visit('http://localhost:3000/#/')
    py.contains('Dismiss').click()
    py.get('.cc-compliance').get('a').should().have_text('Me want it!').click()
    py.get('.mat-paginator-page-size-label').scroll_into_view()
    # hide element so dropdown can be clicked
    el = py.get('.cc-window.cc-floating.cc-type-info.cc-theme-classic.cc-bottom.cc-right')
    py.webdriver.execute_script("arguments[0].style.visibility='hidden'", el.webelement)
    # open items per page dropdown
    py.get('#mat-select-0').click()
    assert py.wait(5).until(lambda driver: driver.find_element(By.ID, 'mat-select-0-panel').is_displayed() == True)
    assert py.get('#mat-option-0').should().be_visible()
    assert py.get('#mat-option-0').is_displayed()
    assert py.get('#mat-option-1').should().be_visible()
    assert py.get('#mat-option-1').is_displayed()
    assert py.get('#mat-option-2').should().be_visible()
    assert py.get('#mat-option-2').is_displayed()
    x = randint(0, 2)
    # select something based on andom value 0, 1 or 2
    if x == 0:
        py.get('#mat-option-0').click()
        py.get('.mat-paginator-page-size-label').scroll_into_view()
        assert py.contains('King of the Hill').should().be_visible()
        assert py.contains('King of the Hill').is_displayed()
    elif x == 1:
        py.get('#mat-option-1').click()
        py.get('.mat-paginator-page-size-label').scroll_into_view()
        assert py.contains(' OWASP Juice Shop T-Shirt ').should().be_visible()
        assert py.contains(' OWASP Juice Shop T-Shirt ').is_displayed()
    else:
        py.get('#mat-option-2').click()
        py.get('.mat-paginator-page-size-label').scroll_into_view()
        assert py.contains('Woodruff Syrup').should().be_visible()
        assert py.contains('Woodruff Syrup').is_displayed()
