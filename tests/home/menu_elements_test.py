def test_menu_elements(py):
    py.visit('http://localhost:3000/#/')
    py.contains('Dismiss').click()
    py.get('[aria-label="Open Sidenav"]').click()
    assert py.contains('Contact').should().be_visible()
    assert py.contains('feedback').should().be_visible()
    assert py.get('[aria-label="Go to contact us page"]').should().be_visible()
    assert py.contains(' Customer Feedback ').should().be_visible()
    assert py.contains('Company').should().be_visible()
    assert py.contains(' business_center ').should().be_visible()
    assert py.get('[aria-label="Go to about us page"]').should().be_visible()
    assert py.contains(' About Us ').should().be_visible()
    assert py.contains(' camera ').should().be_visible()
    assert py.get('[aria-label="Go to photo wall"]').should().be_visible()
    assert py.contains(' Photo Wall ').should().be_visible()
    assert py.contains(' school ').should().be_visible()
    assert py.get('[aria-label="Launch beginners tutorial"]').should().be_visible()
    assert py.contains(' Help getting started ').should().be_visible()
    assert py.get('[data-icon="github"]').should().be_visible()
    assert py.get('[aria-label="Go to OWASP Juice Shop GitHub page"]').should().be_visible()
    assert py.contains(' GitHub ').should().be_visible()
    # get class, get tag that contains text
    assert py.get('.appVersion').get('span').contains('OWASP Juice Shop').should().be_visible()
    assert py.contains('v14.2.0').should().be_visible()
    # icons
    assert py.get('.icon-angular').should().be_visible()
    assert py.get('.icon-html5').should().be_visible()
    assert py.get('.icon-sass').should().be_visible()
    assert py.get('.icon-css3').should().be_visible()
    assert py.get('.icon-javascript-alt').should().be_visible()
    assert py.get('.icon-nodejs').should().be_visible()
    assert py.get('.icon-database-alt2').should().be_visible()
    assert py.get('.icon-mongodb').should().be_visible()
